#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Apr  7 13:48:28 2020

@author: rayricher
"""

"""
Page principale du programme. Contient les modules de mise en forme et 
d'enregistrement des simulations. Allo !
"""

from fichier_carte import *
from banque_de_carte import *
from paquet import *
from fichier_joueur import *
from objets_fondamentaux import *


ange = Carte('Archangel of Thunes')
ange.set_manacost([3,'W','W'])


soulsister = Carte('Soulmender')
soulsister.set_manacost(['W'])

topter = Carte('topter')
topter.set_manacost()

plaine = Land('Plaine')
plaine.ajouter_AA(Mana(), Mana(W=1))

Ange = Paquet('Deck Ange')
Ange.ajouter_carte(soulsister,1)
Ange.ajouter_carte(ange,1)
Ange.ajouter_carte(plaine,4)

tom = Joueur('Thomas', Ange)

tom.mélanger()
tom.piger(6)
tom.jouer(plaine, Mana())
tom.montrer_plateau()
tom.activer_AA(plaine,1)

