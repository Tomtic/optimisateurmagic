#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Apr  7 13:55:05 2020

@author: rayricher
"""

"""
Dossier qui contient les objects nécéssaires pour que le programme fonctionne
"""

"""
Carte. Objet qui contient plusieurs propriétés. Un booléen qui détermine l'état tap
ou untap. Un CMC qui contient le coût total de la carte en mana et une liste «manacost» qui 
détail le coût en mana de la carte. 
"""
from fichier_carte import *
from paquet import *
from outils import *
from objets_fondamentaux import *

class Joueur:
    
    def __init__(self, nom, paquet):
        
        self.nom = nom                  #Nom du joueur
        self.paquet_partie = paquet.liste_paquet   #Paquet qui a été fournis à la création
        self.paquet_original = []
        self.paquet_original += paquet.liste_paquet     #Paquet qui sera utilisé par le programme
        self.main = []                  #Liste des cartes dans la main du joueur
        self.défausse = []              #Liste des cartes dans la défausse du joueur
        self.plateau = []               #Liste des cartes sur le plateau du joueur
        self.exile = []                 #Liste des cartes en exile du joueur
        
        """
        Gestion de la réserve de mana
        """
        
        self.manapool = Mana()       
        self.CMP = 0 #Converted manapool
    
    """ 
    Attribut qui permet de mélanger le paquet en créant une liste qui additionne 
    aléatoirement des éléments de la liste originale de carte fournis.
    """
    def mélanger(self):
        
        import numpy as np
        
        #Méthode qui brasse de manière aléatoire le paquet_temp
        np.random.shuffle(self.paquet_partie)

    
    """
    Attributs temporaires qui permettent de visualiser l'état de différent
    éléments utilisés par le programme. 
    """
    def montrer_liste(self):
        
        print([i.nom for i in self.paquet_partie])      
        print([i.nom for i in self.paquet_original])
        
    def montrer_main(self):
        
        print([i.nom for i in self.main])
    
    def montrer_plateau(self):
        
        print([i.nom for i in self.plateau])
    """
    Attribut qui permet de piger une carte du dessus de la liste paquet_jouer et de l'ajouter 
    à la liste main, enlève cette carte de la liste paquet_partie
    """
    def piger(self, nb=1):
        
        for i in range(nb):
            
            self.main += [self.paquet_partie.pop(0)]

        
    """
    Attribut qui permet de placer une carte de sa main 
    en jeux en payant son CMC. pour l'instant, la mana de couleur sera ajouté
    plus tard.
    """
    
    def jouer(self, carte, cout_mana):
        
        """On doit dabord vérifier que la carte peut être jouer de facon valide.
        On utilise la fonction valider_cout_mana.
        """
        
        #La carte est-elle présente dans la main du joueur ?
        if not carte in self.main:
            print("""{} n'est pas dans votre main !""".format(carte.nom))
        
        #On vérifie dans les deux prochains conditionnel si la mana fournit
        #correspond minimalement au manacost de la carte
        elif not valider_cout_mana(carte, self.manapool, cout_mana):
            print("""Vous n'avez pas une réserve de mana suffisante pour jouer {}""".format(carte.nom))
    
        else:
            """
            On peut finalement jouer la carte. On paie la mana, et change la carte de liste
            """
            
            for i in list(cout_mana.mana.keys()):
                self.manapool.mana[i] -= cout_mana.mana[i]
            
            self.main.remove(carte)
            self.plateau += [carte]


    def activer_AA(self, carte, num):
        
        """ On désire activer une abilité activable. Il faut vérifier le coût et si la 
        carte se trouve en jeu. Puis il faut vérifier l'effet 
        """
        active = False
        AA_activer = carte.AA.get(num)
        #On vérifie si la carte est en jeu
        if not carte in self.plateau:
            print("""{} n'est pas en jeu !""".format(carte.nom))
        
        #On vérifie si le coût est de la mana
        elif isinstance(AA_activer[0], Mana):
            
            #On vérifie si la mana correspond 
            if not valider_cout_mana(carte, self.manapool, AA_activer[0]):
                print("""Vous n'avez pas une réserve de mana suffisante pour activer cette abilité""")
            
            else:
                #On active l'abilité 
                active = True
        
        if active:
            print('activer')
        



















