#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Apr 14 19:43:06 2020

@author: rayricher
"""
"""
Ce fichier contient plusieurs objets fondamentaux et récurent qui sont utile dans
de multiple cas de figure. L'attribut important de ses objets est leurs classe qui permet
de les distinguer aisément les uns des autres. 
"""

    
    
class Mana():
    
    def __init__(self, W=0, R=0, G=0, B=0, U=0, C=0, *args, **kwargs):
        
        super().__init__(*args, **kwargs)
        
        self.mana = {      
                'W':W,
                'B':B,
                'R':R,
                'G':G,
                'U':U,
                'C':C
                }
       
        #On désire une version liste par symbole
        self.manaliste = []
        for i,j in self.mana.items():
            self.manaliste += j*[i]

class Vie():
    
    def __init__(self, pv=0, *args, **kwargs):
        
        super().__init__(*args, **kwargs)
        
        self.pv = pv

 
class Cout(Mana, Vie):
    
    def __init__(self, *args, **kwargs):
        
        super(Cout, self).__init__(*args, **kwargs)  

a =  Cout(W=1, R=3, pv=3) 
print(a.pv)
print(a.mana)     

