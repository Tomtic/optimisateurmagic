#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Apr  9 17:41:23 2020

@author: rayricher
"""
from fichier_carte import *

"""
Ce fichier est un grand dictionnaire qui contient toute les cartes actuellement
construite et fonctionnel
"""

#Dictionnaire générale
Banque_carte= {}

Banque_carte['plaine'] = Carte('Plaine')
Banque_carte['île'] = Carte('Île')

